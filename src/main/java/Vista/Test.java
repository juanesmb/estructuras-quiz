package Vista;

import Modelo.Estudiante;
import ufps.util.colecciones_seed.ListaS;
import ufps.util.colecciones_seed.Nodo;

/**
 *
 * @author juan Moncada
 */
public class Test 
{
    public static void main(String[] args) {
        
        Estudiante a = new Estudiante(5,"alvaro");
        Estudiante b = new Estudiante(0,"bacily");
        Estudiante c = new Estudiante(-1,"camila");
        
        ListaS<Estudiante> l = new ListaS ();
        
        l.insertarAlFinal(a);
        l.insertarAlFinal(b);
        l.insertarAlFinal(c);
        
        try
        {
            l.eliminarNodoMayor();
        }catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
        
        
        
        
    }
}
